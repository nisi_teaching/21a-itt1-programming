![Build Status](https://gitlab.com/nisi_teaching/21a-itt1-programming/badges/master/pipeline.svg)


# 21A-ITT1-PROGRAMMING

weekly plans, resources and other relevant stuff for the course. Mostly online due to Covid-19

Links:

* [gitlab site](https://nisi_teaching.gitlab.io/21a-itt1-programming/)
* [Exercises gitlab group](https://gitlab.com/21a-itt1-programming-exercises)
