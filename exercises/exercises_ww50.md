---
Week: 50
tags:
- Classes
- Objects
- Object oriented programming
- Main guard
---

# Exercises for ww50

## Exercise 1 - Programming exercise - Person class 

### Information

Create and use a python class called Person

### Exercise instructions

1. Create the class in a Python file, name the class Person()
2. Add the following attributes
    1. fullname (string)
    2. birthdate (datetime formatted as YYYYMMDD)
    3. address (list with streetname, streetnumber, city, postal_code, countrycode)
    5. gender (male, female, other)

3. Add the following methods
    1. get_firstname ```(return str(firstname)```
    2. get_lastname ```(return str(lastname)```
    3. get_age ```(return int(age)```
    4. get_address (only return ```list(streetname, streetnumber, city)```)
    5. get_full_info (return all info from the above methods as a dictionary)

4. create at least 3 persons from your class and test your methods 

## Exercise 2 - Programming exercise - Person Class extension

### Information

Extend the Person class with more attributes and methods, name the class Extended_Person()

### Exercise instructions

1. extend the Person class with the following attributes
    1. pet_type (string)
    2. pet_name (string)
    3. monthly_income (float)
2. extend with the following methods
    1. get_pet ```(return str(pet_type + pet_name))```
    2. get_income (if below 10000 return ```str(low)```, if between 10000 and 20000 return ```str(middle)``` if above 20000 return ```str(high)```)
4. create at least 3 persons from your new class and test your methods 

## Exercise 3 - Move classes to an external file

### Information

To be able to reuse you methods in different applications you need to move them into a seperate python file, this is exactly the same as when you are using external python modules.

### Exercise instructions

1. create a file called person.py
2. copy both the Person and Extended_Person classes to the file
3. below the classes create a function called main() and copy your method tests to this function
4. below the main function add a main guard as in party.py [https://gitlab.com/EAL-ITT/20a-itt1-programming/blob/master/docs/nikolaj_simonsen_programming_exercises/Party.py](https://gitlab.com/EAL-ITT/20a-itt1-programming/blob/master/docs/nikolaj_simonsen_programming_exercises/Party.py) 
5. run person.py and confirm that the main() function runs
6. create a python file called person_test.py in the same directory as person.py
7. import your Extended_Person class with:  
    ```
    from person.py import Extended_Person
    ```
8. create at least 3 persons from your Extended_Person and test your methods in the same way as in exercise 1 

\pagebreak