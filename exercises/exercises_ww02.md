---
Week: 02
tags:
  - Python quiz
  - Your own challenge
---

# Exercises for ww02

## Exercise 1 - Python quiz

### Information

It is important to notice your own progress when learning.
In week 44 we did the Python quiz at w3schools and we are going to do it again today.

The purpose is to reflect on the progress you have been making, regarding Python, since week 44.

### Exercise instructions

Complete the Python quiz [https://www.w3schools.com/python/python_quiz.asp](https://www.w3schools.com/python/python_quiz.asp)

You are not allowed to google for answers.
When the quiz is done, review your answers and compare with your answers from week 43.

You have 15 minutes.

## Exercise 2 - Recap overview

### Information

This exercise helps you to get an overview of what you need to recap before the exam

### Exercise instructions

1. Create a .md document to keep track of the course learning goals from weekly plans  
   Suggested document layout:

   ```md
   - Week xx - xxxxxxx
     - Learning goal x:
       - [ ] level 1:
       - [ ] level 2:
       - [ ] level 3:
     - Learning goal y:
       - [ ] level 1:
       - [ ] level 2:
       - [ ] level 3:
   - Week yy - yyyyy
     - Learning goal z:
       - [ ] level 1:
       - [ ] level 2:
       - [ ] level 3:
     - Learning goal p:
       - [ ] level 1:
       - [ ] level 2:
       - [ ] level 3:
   ```

2. In your document extract learning goals from all weeks, including levels.
3. Take some time to acces yourself for all learning goals, notig which levels you meet and which you are missing.

## Exercise 3 - Recap and quiz.

### Information

This exercise is the actual recap

### Exercise instructions

According to your self assesment in exercise 1, read material and do exercises from the weeks that you missed learning goals.

When you are done, you can also take this self-assessment quiz that follows a similar structure to the exam.
[Quiz on Google Forms](https://forms.gle/qrqEEAYVvwVMTpLB6).

If you want to make it even more realistic, set a timer for 15 minutes to complete the quiz. In that way, you have the same amount of time. Do note that the questions are different from in the exam, therefore it is no true indicator of how well you will fare in the exam. See this as a practice of form.
