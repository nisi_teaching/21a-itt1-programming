---
Week: 46
tags:
- grep
- Regular expressions
---

# Exercises for ww46

## Exercise 1 - PY4E chapter 11 knowledge (group) 

### Information

In your team discuss your understanding of:

1. What is grep capable of ?
2. What are regular expressions ?
3. What Python library can be used for regular expressions ?
4. Explain what character matching in regular expressions does ? 
5. What does the `re.search()` method return ? 
6. What does the `re.findall()` method return ? 
7. What is the escape character used for in regular expressions ?
8. What does the `$` match ?

### Exercise instructions

Agree in your team on answers and complete the quiz at: [https://forms.gle/fiMxbCkiKRc9TsKJ7](https://forms.gle/fiMxbCkiKRc9TsKJ7)

You have 30 minutes to complete exercise 0

*Remember to note your answers in your team's shared document*

## Exercise 2 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 11 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

\pagebreak