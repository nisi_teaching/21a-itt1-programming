---
Week: 51
tags:
- Your own challenge
- Catch up
---

# Exercises for ww51

## Exercise 1 - PY4E chapter 16 exercises 

### Information

This week programming lectures are without lecturer.

The author of PY4E has created several videos to help you complete the exercises from chapter 16.
Chapter 16 os about visualizing data in different ways.

### Exercise instructions

1. Read chapter 16
2. Solve the exercises from chapter 16
3. Back up your code to gitlab

### Links  

PY4E Chapter 16 video lessons:

* part 1 [https://youtu.be/gy6l4DTv1KU](https://youtu.be/gy6l4DTv1KU)
* part 2 [https://youtu.be/x2KMFkvDtI8](https://youtu.be/x2KMFkvDtI8)
* part 3 [https://youtu.be/vE0_PM2BOiU](https://youtu.be/vE0_PM2BOiU)

PY4E Chapter 16 worked examples:  

* geodata [https://youtu.be/mt03Z2QjndQ](https://youtu.be/mt03Z2QjndQ)
* page rank - spidering [https://youtu.be/SbtGozLk5Yk](https://youtu.be/SbtGozLk5Yk)
* page rank - computation [https://youtu.be/9gtLOS87ZPs](https://youtu.be/9gtLOS87ZPs)
* page rank - visualization [https://youtu.be/vM4kJNePwhI](https://youtu.be/vM4kJNePwhI)
* gmane / mail - retrieval [https://youtu.be/NFS5MLKdNGw](https://youtu.be/NFS5MLKdNGw)
* gmane / mail - model [https://youtu.be/8GHtT7GqRKk](https://youtu.be/8GHtT7GqRKk)
* gmane / mail - visualization [https://youtu.be/AQtKm02FQq4](https://youtu.be/AQtKm02FQq4) 


\pagebreak