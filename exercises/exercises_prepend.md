---
title: '21A-ITT1-programming'
subtitle: 'Exercises'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: '21A-ITT1-programming, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans.

References  

* Lecture plan, weekly plans, exercises [https://eal-itt.gitlab.io/20a-itt1-programming/](https://eal-itt.gitlab.io/21a-itt1-programming)

* Exercises hand-in: Your gitlab projects, In the gitlab group [https://gitlab.com/20a-itt1-programming-exercises](https://gitlab.com/21a-itt1-programming-exercises)
