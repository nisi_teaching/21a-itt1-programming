---
Week: 50
tags:
- API
- XML
- JSON
- Web services
---

# Exercises for ww49
 
## Exercise 1 - PY4E chapter 13 knowledge sharing (Group) 

### Information

In your team discuss your understanding of:

1. How is the XML format structured?
2. How is JSON structured and what Python datatype does it resemble?
3. What Python module is used to parse JSON?
4. What is an API ?
5. Can you access all API's? if no, why?
6. What is SOA and why is it kool?
7. Give an example of combining data from 2 different API's in a single application (remember that you can both GET and POST)

### Exercise instructions

Agree in your team on answers and complete the quiz at: [https://forms.gle/LzfMpRE3usxchvpt8](https://forms.gle/LzfMpRE3usxchvpt8)

You have 30 minutes to complete exercise 0

*Remember to note your answers in your team's shared document*

## Exercise 2 - PY4E chapter 13 Exercise 1 

### Information

Use pair programming to solve the first exercise from chapter 13 in PY4E

### Exercise instructions

1. Team up in pairs
2. Download Application 1: Google geocoding web service [https://www.py4e.com/code3/geojson.py](https://www.py4e.com/code3/geojson.py)
3. Test and verify the application works as intended.
4. Rewrite the exercise instruction in to **Pseudo code** (logical steps in you program)  

    Exercise 1: Change geojson.py to print out the two-
    character country code from the retrieved data. Add error checking so
    your program does not traceback if the country code is not there. Once
    you have it working, search for “Atlantic Ocean” and make sure it can
    handle locations that are not in any country.

5. Write the program following the logic steps, change role for each logic step

## Exercise 3 - PY4E chapter 13 Exercise 2 

### Information

This exercise requires a twitter account, if you do not have that either make one or skip to exercise 4.

Use pair programming to solve the third exercise from chapter 12 in PY4E

### Exercise instructions

1. Team up in pairs
2. Download the needed files for Application 2: Twitter [http://www.py4e.com/code3](http://www.py4e.com/code3)
3. Test and verify the application works as intended.
2. Rewrite the exercise instruction in to **Pseudo code** (logical steps in you program)
3. Write the program following the logic steps, change role for each logic step

## Exercise 4 - Reading and Parsing a RSS feed

### Information

RSS feeds are available for gitlab groups, they contain information about latest commits to the project included in the group.

Use pair programming to write a Python application that can read an RSS feed from gitlab.

### Exercise instructions

1. Install the feedparser module [https://pypi.org/project/feedparser/](https://pypi.org/project/feedparser/) in a Python project using either PyCharm or PIP ```pip install feedparser``` depending on you IDE. 
2. import the feedparser module into you python program
3. Go to the EAL-ITT gitlab group activity page  [https://gitlab.com/groups/EAL-ITT/-/activity](https://gitlab.com/groups/EAL-ITT/-/activity) and copy the RSS feed link (look for the RSS icon). It should look something like this:  
```https://gitlab.com/EAL-ITT.atom```
4. Make a variable called eal_itt and assign the RSS link to it 
5. read the feed from gitlab using ```rss_feed = feedparser.parse(eal_itt)```
6. print(rss_feed) to view the contents
7. Examine the rss_feed by setting a breakpoint and debugging you program, the entries list holds the activity entries.
8. For each of the 5 newest entries parse and print the rss_feed.entries:
    1. entry author
    2. entry updated
    3. commit message (hint! it is in the entry title_detail, use `beautiful soup` to extract the commit message from html) 

feedparser documentation [https://pythonhosted.org/feedparser/](https://pythonhosted.org/feedparser/)  

Using the JSON module you can print the feed entries in a readable way using `print(json.dumps(rss_feed.entries, indent = 1))`

\pagebreak