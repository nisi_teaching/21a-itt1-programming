---
Week: 41
tags:
- Debugging
- Files
- String
---

# Exercises for ww41

## Exercise 1 - PY4E chapter 6 knowledge (group)

### Information

This exercise introduces, recaps and shares knowledge, on a group level, about PY4E chapter 6.

### Exercise instructions

1. Either/or:
    * Read chapter 6
    * Watch chapter 6 videos 

2. In your team discuss your understanding of:

    * How do you access individual characters in a string ? 
    * How can you traverse a string ?
    * What is a string slice ?
    * What does it mean, that strings are immutable ?
    * How do you compare two strings ?
    * What string methods do you know ?
    * How do you extract a substring from a string

3. Agree in your team on answers and complete the quiz at: [https://forms.gle/LoN989gnFHurBLKb7](https://forms.gle/LoN989gnFHurBLKb7)

*You have 45 minutes to read and 15 minutes to complete the quiz.*

*Remember to also note your answers in your team's shared document* 

## Exercise 2 - Python for everybody chapter 6 exercises 1, 2, 3 

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 6 exercise 1, 2 and 3, in Python For Everybody.

**Tip!** Chapter 6, exercises 1 - 4 is embedded in the chapter text, exercise 5 and 6 is at the end of the chapter.  

**Remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

## Exercise 3 - PY4E chapter 7 knowledge (group)

### Information

This exercise introduces, recaps and shares knowledge, on a group level, about PY4E chapter 6.

### Exercise instructions

1. Either/or:
    * Read chapter 7
    * Watch chapter 7 videos 

2. In your team discuss your understanding of:  

    * What is a file handle ?
    * How do you count lines in a file ?
    * How do you search through a file ?
    * What does the `rstrip` method do ?
    * What happens if you try to open a non existing file ?
    * How do you write to a file ?

3. Agree in your team on answers and complete the quiz at: [https://forms.gle/VgUJ3P7fnj2Gx9xn8](https://forms.gle/VgUJ3P7fnj2Gx9xn8)

*You have 45 minutes to read and 15 minutes to complete the quiz.*

**Remember to also note your answers in your team's shared document**

## Exercise 4 - Python for everybody chapter 7 exercises 

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 7 exercises, in Python For Everybody.  
  
**Remember to also document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**


## Exercise 5 - Python for everybody chapter 6 remaining exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete the remaining exercises from chapter 6, in Python For Everybody.

**Tip!** Chapter 6, exercises 1 - 4 is embedded in the chapter text, exercise 5 and 6 is at the end of the chapter.  

**Remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

## Exercise 6 - Optional challenge exercise

### Information

This exercise is optional and meant as a challenge for those that needs more than the above exercises.  
The challenge is to expand the calculator program from ww40 to be able to convert between different number systems.  
Create new functions to convert between binary, hexadecimal and decimal values.

### Exercise instructions

1. create a function that takes a binary number as an argument and returns the number converted to decimal
2. create a function that takes a haxadecimal number as an argument and returns the number converted to decimal
3. create a function that takes a decimal number as an argument and returns the number converted to binary
4. create a function that takes a decimal number as an argument and returns the number converted to hexadecimal
4. create a function that takes a binary number as an argument and returns the number converted to hexadecimal
4. create a function that takes a hexadecimal number as an argument and returns the number converted to binary
5. modify the main program to prompt the user at program start if they want to convert or calculate ie.  
'would you like to: 1. convert numbers 2. calculate numbers'  
If convert is chosen prompt for conversion type ie. 'would you like to convert: 1. decimal to binary 2. decimal to hexadecimal 3. binary to decimal 4. hexadecimal to binary 5. binary to hexadecimal or 6. hexadecimal to binary'
5. add exeption handling to each function to check for correct number format (hexadecimal, decimal and binary) using try/except 



\pagebreak