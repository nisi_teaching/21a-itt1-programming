beautifulsoup4==4.9.1
bs4==0.0.1
importlib-metadata==1.7.0
Jinja2==2.11.2
Markdown==3.2.2
MarkupSafe==1.1.1
soupsieve==2.0.1
zipp==3.1.0
