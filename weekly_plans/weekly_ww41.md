---
Week: 41
Content:  Strings, Files
Material: See links in weekly plan
Initials: NISI
---

# Week 41 ITT1-programming - Strings, Files

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 6 + 7 exercises 
* Read chapter 6 + 7 in Python For Everybody

### Learning goals

The student can:

* Parse strings
* Use string methods
* Use substrings
* Open/read/write/close files
* Parse files
* Handle file exceptions 

The student knows:

* The string type

## Deliverables

* Chapter 6 + 7 exercises, in Python For Everybody, documented on Gitlab

## Schedule

* 8:15 Introduction to the day (K1)
* 8:30 Code review (K1) 
    Code review of programming exercises from ww40
* 9:30 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:45 Exercises without lecturer (K2/K3)
* 15:30 End of day


## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww41](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww41)

## Comments

PY4E Chapter 6 video lessons:

* part 1 [https://youtu.be/dr98iM4app8](https://youtu.be/dr98iM4app8)
* part 2 [https://youtu.be/bIFpJ-qZ3Cc](https://youtu.be/bIFpJ-qZ3Cc)

PY4E Chapter 7 video lessons:

* part 1 [https://youtu.be/9KJ-XeQ6ZlI](https://youtu.be/9KJ-XeQ6ZlI)
* part 2 [https://youtu.be/0t4rvnySKR4](https://youtu.be/0t4rvnySKR4)