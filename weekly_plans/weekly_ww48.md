---
Week: 48
Content:  Networked programs
Material: See links in weekly plan
Initials: NISI
---

# Week 48 ITT1-programming - Networked programs

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 12 exercises 

### Learning goals

The student can:

* Use TCP sockets and libraries to get data via HTTP and HTTPS
* Develop programs using pair programming
* Reflect on own learning

The student knows: 

* Basics of TCP and HTTP(S)

## Deliverables

* Chapter 12 exercises, in Python For Everybody, documented on Gitlab

## Schedule

* 8:15 Introduction to the day (K1)
* 8:30 Code review (K1) 
    Code review of chapter 11 exercises from py4e
* 9:00 Video on networked programs
* 9:30 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 12
    * Watch chapter 12 videos 
* 10:30 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww48](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww48)

## Comments

PY4E Chapter 12 video lessons:

* part 1 [https://youtu.be/RsnaRPC52G0](https://youtu.be/RsnaRPC52G0)
* part 2 [https://youtu.be/Bvx7vY454xw](https://youtu.be/Bvx7vY454xw)
* part 3 [https://youtu.be/Lr9Vm-VghAk](https://youtu.be/Lr9Vm-VghAk)
* part 4 [https://youtu.be/-cmlmaVSONg](https://youtu.be/-cmlmaVSONg)
* part 5 [https://youtu.be/k1sUxGPpQOk](https://youtu.be/k1sUxGPpQOk)
* part 6 [https://youtu.be/D7ZI8--qbBw](https://youtu.be/D7ZI8--qbBw)

PY4E Chapter 12 worked examples:
* socket [https://youtu.be/EqUyu8ZZYUE](https://youtu.be/EqUyu8ZZYUE)  
* urllib [https://youtu.be/jKaCKIdIoks](https://youtu.be/jKaCKIdIoks)  
* beautifulsoup [https://youtu.be/mhaHWiSPxxE](https://youtu.be/mhaHWiSPxxE)  