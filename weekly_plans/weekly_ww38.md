---
Week: 38
Content:  Conditionals
Material: See links in weekly plan
Initials: NISI
---

# Week 38 ITT1-programming - Conditionals

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 3 in Python For Everybody
* Chapter 3 exercises, in Python For Everybody, completed 
* Create a flowchart in collaboration with your team

### Learning goals

* Flowcharts (LGK5, LGK7, LGK8, LGS2, LGS3, LGC1, LGC2)
    * Level 1: The student knows the basic flowchart symbols
    * Level 2: The student knows how a flowchart is organized
    * Level 3: The student can use a flowchart to plan a program for a simple IoT sensor system

* PY4E Chapter 3 (LGK7, LGK8, LGS2, LGC1)
    * Level 1: The student knows what conditional execution, logical operators and exception handling
    * Level 2: The student can implement conditional execution, logical operators and exception handling in seperate program examples
    * Level 3: The student can implement conditional execution, logical operators and exception handling in the same program

## Deliverables

* Chapter 3 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Online at zoom: [https://ucldk.zoom.us/j/62627436603](https://ucldk.zoom.us/j/62627436603) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 8:15 Introduction to the day (K1)
* 8:30 Code review (K1) 
    Code review of chapter 2 exercises from py4e
* 9:00 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 3
    * Watch chapter 3 videos 
* 10:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises without lecturer (K2/K3)
* 15:30 End of day


## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww38](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww38)

## Comments

PY4E Chapter 3 video lessons:

* part 1 [https://youtu.be/2aA3VBdcl6A](https://youtu.be/2aA3VBdcl6A)
* part 2 [https://youtu.be/OczkNrHPBps](https://youtu.be/OczkNrHPBps)

Socratica videos covering some topics from chapter 2 and 3:

* [If, Then, Else in Python](https://youtu.be/f4KOjWS_KZs)
* [Exceptions in Python](https://youtu.be/nlCKrKGHSSk)