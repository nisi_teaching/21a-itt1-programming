---
Week: 47
Content:  Your own challenge, catch up
Material: See links in weekly plan
Initials: NISI
---

# Week 47 ITT1-programming - Your own challenge + catch up (without lecturer)

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Work on your own challenge
* Catch up on missed exercises from previous weeks 

### Learning goals

* Described in previous weekly plans

## Deliverables

## Schedule

This day is without lecturer and planning of the day is suggested as

* 08:15 Exercises without lecturer (K2/K3)
* 11:30 Lunch break
* 12:15 Exercises without lecturer (K2/K3)
* 15:30 End of day


## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww46](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww46)

## Comments

* None