---
Week: 46
Content:  Regular Expressions
Material: See links in weekly plan
Initials: NISI
---

# Week 46 ITT1-programming - Regular Expressions

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 11 exercises

### Learning goals

The student can:

* Use regular expressions

The student knows: 

* Basic syntax of regular expressions
* When to use regular expressions

## Deliverables

* Chapter 11 exercises, in Python For Everybody, documented on Gitlab

## Schedule

* 8:15 Introduction to the day (K1)
* 8:30 Code review (K1) 
    Code review of chapter 10 exercises from py4e
* 9:00 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 11
    * Watch chapter 11 videos 
* 10:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises without lecturer (K2/K3)
* 15:30 End of day


## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww46](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww46)

## Comments

PY4E Chapter 11 video lessons:

* part 1 [https://youtu.be/ovZsvN67Glc](https://youtu.be/ovZsvN67Glc)
* part 2 [https://youtu.be/fiar4QZZ7Xo](https://youtu.be/fiar4QZZ7Xo)
* part 3 [https://youtu.be/GiQdXo2Bvgc](https://youtu.be/GiQdXo2Bvgc)

RegEx online parser and bulder:

* [regex101.com](https://regex101.com/)  
  More exists online, the linked one might not be the best of them ?
