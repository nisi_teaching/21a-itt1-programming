---
Week: 36
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 36 ITT1-programming - Introduction

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Python installed and tested
* Gitlab exercises project created
* Read chapter 1 in Python For Everybody
* Python For Everybody - Chapter 1 exercises completed 
* Chapter 1 exercises, in Python For Everybody, documented on Gitlab

### Learning goals

* Install Python (LGS2, LGC1)
    * Level 1: The student can download Python
    * Level 2: The student can install python
    * Level 3: The student can create a simple stored program

* Gitlab setup (LGK1, LGK4, LGS3, LGC2)
    * Level 1: The student can set up a gitab account
    * Level 2: The student can generate ssh key for gitlab ssh access
    * Level 3: The student can create a gitlab project and use it for simple documentation

* PY4E chapter 1 (LGS2, LGC1)
    * Level 1: The student knows what Python IDLE and REPL is
    * Level 2: The student can use REPL for basic commands
    * Level 3: The student can use IDLE for stored programs

## Schedule

* 9:00 Introduction (K1)
* 10:00 Preparation for exercises (K2)
    Either/or:
    * Read chapter 1 in Python For Everybody
    * Watch chapter 1 videos 
* 10:30 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 12:45 Studycard photo
* 13:45 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Exercises

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww36](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww36)

## Comments

* Introduction slides [https://docs.google.com/presentation/d/10VGoqXbXV6zunfk6lKmUuvXKWMQnGgU6bXgEYwODpYU/edit?usp=sharing](https://docs.google.com/presentation/d/10VGoqXbXV6zunfk6lKmUuvXKWMQnGgU6bXgEYwODpYU/edit?usp=sharing)

PY4E Chapter 1 video lessons:

* part 1 [https://youtu.be/fvhNadKjE8g](https://youtu.be/fvhNadKjE8g)
* part 2 [https://youtu.be/VQZTZsXk8sA](https://youtu.be/VQZTZsXk8sA)
* part 3 [https://youtu.be/LLzFNlCjTSo](https://youtu.be/LLzFNlCjTSo)
* part 4 [https://youtu.be/gsry2SYOFCw](https://youtu.be/gsry2SYOFCw)
