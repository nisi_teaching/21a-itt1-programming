---
Week: 51
Content: Your own challenge, catch up
Material: See links in weekly plan
Initials: NISI
---

# Week 51 ITT1-programming - Visualizing data + catch up (without lecturer)

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Complete exercises from PY4E chapter 16
* Catch up on missed exercises from previous weeks 

### Learning goals

* The student can visualize data in different ways

## Deliverables

## Schedule

This day is without lecturer and planning of the day is suggested as

* 08:15 Work on either PY4E chapter 16 or catch up on exercises (K2/K3) 
* 11:30 Lunch break
* 12:15 Work on either PY4E chapter 16 or catch up on exercises (K2/K3)
* 15:30 End of day

## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww51](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww51)

## Comments

PY4E Chapter 16 video lessons:

* part 1 [https://youtu.be/gy6l4DTv1KU](https://youtu.be/gy6l4DTv1KU)
* part 2 [https://youtu.be/x2KMFkvDtI8](https://youtu.be/x2KMFkvDtI8)
* part 3 [https://youtu.be/vE0_PM2BOiU](https://youtu.be/vE0_PM2BOiU)

PY4E Chapter 16 worked examples:  

* geodata [https://youtu.be/mt03Z2QjndQ](https://youtu.be/mt03Z2QjndQ)
* page rank - spidering [https://youtu.be/SbtGozLk5Yk](https://youtu.be/SbtGozLk5Yk)
* page rank - computation [https://youtu.be/9gtLOS87ZPs](https://youtu.be/9gtLOS87ZPs)
* page rank - visualization [https://youtu.be/vM4kJNePwhI](https://youtu.be/vM4kJNePwhI)
* gmane / mail - retrieval [https://youtu.be/NFS5MLKdNGw](https://youtu.be/NFS5MLKdNGw)
* gmane / mail - model [https://youtu.be/8GHtT7GqRKk](https://youtu.be/8GHtT7GqRKk)
* gmane / mail - visualization [https://youtu.be/AQtKm02FQq4](https://youtu.be/AQtKm02FQq4) 
