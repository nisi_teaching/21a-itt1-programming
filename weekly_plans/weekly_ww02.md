---
Week: 02
Content: Recap and evaluation
Material: See links in weekly plan
Initials: AMNI1
---

# Week 02 ITT1-programming - Recap and evaluation

## Goals of the week(s)

Pratical and learning goals for the period is as follows

### Practical goals

- Complete exercises from the semester you haven't completed

### Learning goals

- None

## Deliverables

- Exercises completed and documented on gitlab

## Schedule

Zoom link for Monday: [https://ucldk.zoom.us/j/6676264172?pwd=a0JPOUR6WEJQQnJSNDJUQmdBbnJCQT09](https://ucldk.zoom.us/j/6676264172?pwd=a0JPOUR6WEJQQnJSNDJUQmdBbnJCQT09)

Zoom link for Friday: [https://ucldk.zoom.us/j/6676264172?pwd=a0JPOUR6WEJQQnJSNDJUQmdBbnJCQT09
](https://ucldk.zoom.us/j/6676264172?pwd=a0JPOUR6WEJQQnJSNDJUQmdBbnJCQT09)

Password: 1234

- 8:15 Introduction to the day (K1)
- 8:45 Python quiz individual (K4)
- 09:00 Recap + Q&A based on your questions (K1/K4)
- 11:30 Lunch break
- 12:15 Catchup on exercises, prepare for exam (with lecturer, K1/K4)
- 13:45 End of programming course

## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww02](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww02)

## Comments
