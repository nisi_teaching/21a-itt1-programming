---
Week: 40
Content:  Iteration
Material: See links in weekly plan
Initials: NISI
---

# Week 40 ITT1-programming - Iteration

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 5 in Python For Everybody
* Chapter 5 exercises, in Python For Everybody, completed

### Learning goals

The student can implement:

* Iteration
* Loops
* While
* For

The student knows:

* Loop patterns
* Debugging by bisection

## Deliverables

* Chapter 5 exercises, in Python For Everybody, documented on Gitlab
* Pair programming calculator challenge - documented on Gitlab

## Schedule

Monday 2021-10-04 (Both classes)  

Online at zoom: [https://ucldk.zoom.us/j/62627436603](https://ucldk.zoom.us/j/62627436603) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 8:15 Introduction to the day (K1)
* 8:30 Code review (K1) 
    Code review of chapter 4 exercises from py4e
* 9:00 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 5
    * Watch chapter 5 videos 
* 10:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises with lecturer (K4)
* 13:45 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww40](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww40)

## Comments

PY4E Chapter 5 video lessons:

* part 1 [https://youtu.be/FzpurxjwmsM](https://youtu.be/FzpurxjwmsM)
* part 2 [https://youtu.be/5QDrj5ogPYc](https://youtu.be/5QDrj5ogPYc)
* part 3 [https://youtu.be/xsavQp8hd78](https://youtu.be/xsavQp8hd78)
* part 4 [https://youtu.be/yjlMMwf9Y5I](https://youtu.be/yjlMMwf9Y5I)