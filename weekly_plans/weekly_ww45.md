---
Week: 45
Content:  Tuples
Material: See links in weekly plan
Initials: NISI
---

# Week 45 ITT1-programming - Tuples

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 10 exercises
* Work on your own challenge (if you have extra time)

### Learning goals

The student can:

* Use the tuples datatype
* Use 3rd party modules
* Use matplotlib to visualize data
* Create and use virtual envronments at a basic level

The student knows: 

* The difference between tuples and othe Python datatypes
* The purpose of using virtual environments

## Deliverables

* Chapter 10 exercises, in Python For Everybody, documented on Gitlab

## Schedule

* 8:15 Introduction to the day (K1)
* 8:30 Code review (K1) 
    Code review of chapter 9 exercises from py4e
* 9:00 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 10
    * Watch chapter 10 videos 
* 10:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww45](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww45)

## Comments

PY4E Chapter 10 video lessons:

* part 1 [https://youtu.be/CaVhM65wD6g](https://youtu.be/CaVhM65wD6g)
* part 2 [https://youtu.be/FdUdA6o0Ij0](https://youtu.be/FdUdA6o0Ij0)