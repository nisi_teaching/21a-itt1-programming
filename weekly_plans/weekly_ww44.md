---
Week: 44
Content:  Dictionaries
Material: See links in weekly plan
Initials: NISI
---

# Week 44 ITT1-programming - Dictionaries

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Chapter 9 exercises, in Python For Everybody, completed
* Work on your own challenge

### Learning goals

The student can:

* Implement Dictionaries
* Traverse Dictionaries
* Use Dictionary functions
* Build histograms using Dictionaries

The student knows: 

* The difference between Dictionaries and other datatypes
* When to use a Dictionary 

## Deliverables

* Chapter 9 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Friday 2021-11-05 (Both classes)  

Online at zoom: [https://ucldk.zoom.us/j/62627436603](https://ucldk.zoom.us/j/62627436603) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 8:15 Introduction to the day (K1)
* 8:30 Code review (K1) 
    Code review of chapter 8 exercises from py4e
* 9:00 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 9
    * Watch chapter 9 videos 
* 10:00 Chapter 9 QUIZ + Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 13:45 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww44](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww44)

## Comments

PY4E Chapter 9 video lessons:

* part 1 [https://youtu.be/yDDRMb-1cxI](https://youtu.be/yDDRMb-1cxI)
* part 2 [https://youtu.be/LRSIuH94XM4](https://youtu.be/LRSIuH94XM4)
* part 3 [https://youtu.be/ZDjiFB1Ib84](https://youtu.be/ZDjiFB1Ib84)