---
Week: 50
Content:  Object-oriented programming
Material: See links in weekly plan
Initials: NISI
---

# Week 50 ITT1-programming - Object-oriented programming

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Object-oriented programming exercises 

### Learning goals

The student can:

* Create simple classes
* Instantiate objects from classes
* Extend classes

The student knows: 

* What a class is
* Why classes are useful
* Class attributes
* Class methods
* Class constructors
* The ```self``` keyword

## Deliverables

* Chapter 14 exercises, in Python For Everybody, documented on Gitlab

## Schedule

* 8:15 Introduction to the day (K1)
* 8:30 Evaluation  
    * Please fill out this survey [https://forms.gle/CkMUfvbTDLQSFDkaA](https://forms.gle/CkMUfvbTDLQSFDkaA)
* 8:45 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 14
    * Watch chapter 14 videos 
* 9:30 Video: Python Classes and Objects [https://youtu.be/apACNr7DC_s](https://youtu.be/apACNr7DC_s)
* 10:00 Live examples (K1)
* 10:30 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww50](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww50)

## Comments

PY4E Chapter 14 video lessons:

* part 1 [https://youtu.be/u9xZE5t9Y30](https://youtu.be/u9xZE5t9Y30)
* part 2 [https://youtu.be/b2vc5uzUfoE](https://youtu.be/b2vc5uzUfoE)

