---
Week: 49
Content:  Web services
Material: See links in weekly plan
Initials: NISI
---

# Week 49 ITT1-programming - Using web services

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 13 exercises 

### Learning goals

The student can:

* Programatically retreive JSON data
* Programatically communicate with an API
* Parse JSON data

The student knows: 

* What XML is
* What JSON is
* What web services are
* What api's are
* What a service oriented architecture is

## Deliverables

* Chapter 13 exercises, in Python For Everybody, documented on Gitlab

## Schedule

* 8:15 Introduction to the day (K1)
* 8:30 Code review (K1) 
    Code review of chapter 12 exercises from py4e
* 9:00 Preparation for hands-on time (K2)  
    Either/or:
    * Read chapter 13
    * Watch chapter 13 videos 
* 10:00 Exercises with lecturer (K1/K4)
* 11:30 Lunch break
* 12:15 Exercises without lecturer (K2/K3)
* 15:30 End of day

## Hands-on time

For details see the exercise page at [https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww49](https://eal-itt.gitlab.io/21a-itt1-programming/exercises/exercises_ww49)

## Comments

PY4E Chapter 13 video lessons:

* part 1 [https://youtu.be/7NEtEctD9Cw](https://youtu.be/7NEtEctD9Cw)
* part 2 [https://youtu.be/A8KcBx9153Y](https://youtu.be/A8KcBx9153Y)
* part 3 [https://youtu.be/0cA6W-4JPQ4](https://youtu.be/0cA6W-4JPQ4)
* part 4 [https://youtu.be/J5DjteDzgoM](https://youtu.be/J5DjteDzgoM)
* part 5 [https://youtu.be/SNeJcvBY-h4](https://youtu.be/SNeJcvBY-h4)
* part 6 [https://youtu.be/QyHcOL3C7fQ](https://youtu.be/QyHcOL3C7fQ)
* part 7 [https://youtu.be/mrRo2xX39nw](https://youtu.be/mrRo2xX39nw)

PY4E Chapter 13 worked exercises:

* JSON [https://youtu.be/RGQfDirZ7_s](https://youtu.be/RGQfDirZ7_s)
* GeoJSON API [https://youtu.be/vjQZscHOaG4](https://youtu.be/vjQZscHOaG4)
* Twitter API [https://youtu.be/zJzPyEPCbXs](https://youtu.be/zJzPyEPCbXs)


