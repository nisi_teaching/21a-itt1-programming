---
title: '21A-ITT1-programming'
subtitle: 'Weekly plans'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: '21A-ITT1-programming, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the wekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programme for each week of the 21A-ITT1-programming course.