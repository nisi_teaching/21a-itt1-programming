"""
Exercise 1: Download a copy of the file www.py4e.com/code3/words.txt
Write a program that reads the words in words.txt and stores them as
keys in a dictionary. It doesn’t matter what the values are. Then you
can use the in operator as a fast way to check whether a string is in the
dictionary.
"""

# file_name = input('Enter a file name: ')
# file = open('./files/' + file_name)

from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/docs/nikolaj_simonsen_programming_exercises/files/')

words_dict = dict()
i = 0

with open(files_path / 'words.txt' ) as words_file:
    for line in words_file:
        words = line.split()
        for word in words:
            words_dict[word] = [i]
            i += 1

print('programming' in words_dict)
print('student' in words_dict)



