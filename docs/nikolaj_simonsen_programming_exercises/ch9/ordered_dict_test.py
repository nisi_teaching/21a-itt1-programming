groceries = {
    'item1': 'cucumber',
    'item2': 'cheese',
    'item3': 'tomatoes'
}

# print(groceries['item2'])

for key, value in groceries.items():
    print(f'key: {key}. value: {value}')

x = 'item6' in groceries

print('item6' in groceries)