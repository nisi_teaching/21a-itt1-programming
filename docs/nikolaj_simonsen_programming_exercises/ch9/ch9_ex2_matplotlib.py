"""
Exercise 2: Write a program that categorizes each mail message by which day of the week the commit was done.
To do this look for lines that start with “From”, then look for the third word and keep a running
count of each of the days of the week.
At the end of the program print out the contents of your dictionary (order does not matter).

Sample Line:
From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008

Sample Execution:
python dow.py
Enter a file name: mbox-short.txt
{'Fri': 20, 'Thu': 6, 'Sat': 1}
"""

# file_name = input('Enter a file name: ')
# file = open('./files/' + file_name)

from matplotlib import pyplot as plt
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths

mails_dict = dict()
files_path = Path(str(Path.cwd()) + '/docs/nikolaj_simonsen_programming_exercises/files/')
file_to_open = files_path / 'mbox-short.txt'
# print(pathlib.Path.cwd())

with open(file_to_open) as mails_file:
    for line in mails_file:
        if line.startswith("From "):
            linewords = line.split()
            if linewords[2] not in mails_dict:
                mails_dict[linewords[2]] = 1
            else:
                mails_dict[linewords[2]] = mails_dict[linewords[2]] + 1

print(mails_dict)

# Make a list of tuples and sort the list
sort_list = list()
for day, count in list(mails_dict.items()):
    sort_list.append((count, day))
sort_list.sort(reverse=True)

# lists for plot x and y axis
days_list = list()
count_list = list()

# add sorted list items to plot lists:
for count, day in sort_list:
    days_list.append(day)
    count_list.append(count)

plt.style.use("seaborn")
plt.plot(days_list, count_list, color="#444444", label="mails/day")
plt.ylabel("Mails")
plt.xlabel("Day")
plt.title("Mails a day chart")
plt.legend()
plt.tight_layout()
# plt.gcf().subplots_adjust(bottom=0.3)
plt.show()

