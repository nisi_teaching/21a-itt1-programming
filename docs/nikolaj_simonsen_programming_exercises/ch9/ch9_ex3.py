"""
Exercise 3: Write a program to read through a mail log, build a histogram using a dictionary
to count how many messages have come from each email address, and print the dictionary.

Enter file name: mbox-short.txt

{'gopal.ramasammycook@gmail.com': 1, 'louis@media.berkeley.edu': 3,
'cwen@iupui.edu': 5, 'antranig@caret.cam.ac.uk': 1,
'rjlowe@iupui.edu': 2, 'gsilver@umich.edu': 3,
'david.horwitz@uct.ac.za': 4, 'wagnermr@iupui.edu': 1,
'zqian@umich.edu': 4, 'stephen.marquard@uct.ac.za': 2,
'ray@media.berkeley.edu': 1}
"""

# file_name = input('Enter a file name: ')
# file = open('./files/' + file_name)

from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/docs/nikolaj_simonsen_programming_exercises/files/')

mails_histogram = dict()

with open(files_path / 'mbox-short.txt') as mails_file:
    for line in mails_file:
        if line.startswith("From: "):
            line_words = line.split()
            if line_words[1] not in mails_histogram:
                mails_histogram[line_words[1]] = 1
            else:
                mails_histogram[line_words[1]] = mails_histogram[line_words[1]] + 1

print(mails_histogram)



