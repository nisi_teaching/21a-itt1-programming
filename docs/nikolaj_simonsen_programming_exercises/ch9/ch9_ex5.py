"""
Exercise 5: This program records the domain name (instead of the address)
where the message was sent from instead of who the mail came from (i.e., the whole email address).
At the end of the program, print out the contents of your dictionary.

Enter a file name: mbox-short.txt
{'media.berkeley.edu': 4, 'uct.ac.za': 6, 'umich.edu': 7,
'gmail.com': 1, 'caret.cam.ac.uk': 1, 'iupui.edu': 8}
"""
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/docs/nikolaj_simonsen_programming_exercises/files/')

file_name = input('Enter a file name: ')

try:
    mails_file = open(files_path / file_name)
    mails_histogram = dict()

    for line in mails_file:
        if line.startswith("From: "):
            domain = line[line.find("@"):]
            if domain not in mails_histogram:
                mails_histogram[domain] = 1
            else:
                mails_histogram[domain] += 1

    mails_file.close()

    print(mails_histogram)

except FileNotFoundError:
    print("File not found")







