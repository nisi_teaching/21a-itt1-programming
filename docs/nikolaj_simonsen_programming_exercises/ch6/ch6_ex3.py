"""
Exercise 3: Encapsulate this code in a function named count, and generalize
it so that it accepts the string and the letter as arguments
"""


def count(word, letter):
    count = 0
    for character in word:
        if character == letter:
            count += 1
    return count


print('the letter appears ' + str(count(input('input a word: '), input('input a letter: '))) + ' times')
