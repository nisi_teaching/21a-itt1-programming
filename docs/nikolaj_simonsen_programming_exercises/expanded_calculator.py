def add(num1, num2):
    return num1 + num2

def sub(num1, num2):
    return num1 - num2

def div(num1, num2):
    return num1 / num2

def mult(num1, num2):
    return num1 * num2

def dec_to_bin(num1):
    return bin(int(num1))

def dec_to_hex(num1):
    return (hex(int(num1)))

def bin_to_dec(num1):    
    return int(num1,2)

def hex_to_dec(num1):
    return int(num1,16)

def bin_to_hex(num1):
    return hex(int(num1, 2))

def hex_to_bin(num1):
    return bin(int(num1, 16))

def goodbye():
    print('goodbye...')
    exit(0) 

print('\nWelcome to the expanded calculator (write done or press ctrl+c to quit)')

result = 0.0
chosen_operation = ''

while True:
    print('would you like to: 1. convert numbers 2. calculate numbers')
       

    try:  
        operation = input('> ') 
        if operation == 'done':
            goodbye()
        operation = int(operation)

        if operation == 1:
            print('would you like to convert: 1. decimal to binary 2. decimal to hexadecimal 3. binary to decimal 4. hexadecimal to decimal 5. binary to hexadecimal or 6. hexadecimal to binary')
            operation = input('> ')

            if operation == 'done':
                goodbye()

            operation = int(operation)

            if operation == 1:
                print('enter a decimal number to convert to binary')
                number = input('> ')
                if number == 'done':
                    goodbye()  
                result = dec_to_bin(number)
                chosen_operation = ' converted to binary is '
            elif operation == 2:
                print('enter a decimal number to convert to hexadecimal')
                number = input('> ')
                if number == 'done':
                    goodbye()
                result = dec_to_hex(number)
                chosen_operation = ' converted to hexadecimal is '
            elif operation == 3:
                print('enter a binary number to convert to decimal')
                number = input('> ')
                if number == 'done':
                    goodbye()
                result = bin_to_dec(number)
                chosen_operation = ' converted to decimal is '
            elif operation == 4:
                print('enter a hexadecimal number to convert to decimal')
                number = input('> ')
                if number == 'done':
                    goodbye() 
                result = hex_to_dec(number)
                chosen_operation = ' converted to decimal is '

            elif operation == 5:
                print('enter a binary number to convert to hexadecimal')
                number = input('> ')
                if number == 'done':
                    goodbye() 
                result = bin_to_hex(number)
                chosen_operation = ' converted to hexadecimal is '

            elif operation == 6:
                print('enter a hexadecimal number to convert to binary')
                number = input('> ')
                if number == 'done':
                    goodbye() 
                result = hex_to_bin(number)
                chosen_operation = ' converted to binary is '    

            print(f'{number} {chosen_operation} {result}')
            #print(str(number1) + chosen_operation + str(number2) + ' = ' + str(result))
            print('restarting....')

        else:             
            print('enter the first number')
            number1 = input('> ')
            if number1 == 'done':
                goodbye()
            print('enter the second number')
            number2 = input('> ')
            if number2 == 'done':
                goodbye()
            number1 = float(number1)
            number2 = float(number2)
            print('would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?')
            operation = input('> ')
            if operation == 'done':
                goodbye()
            operation = int(operation)
            if operation == 1:
                result = add(number1, number2)
                chosen_operation = 'added with'
            elif operation == 2:
                result = sub(number1, number2)
                chosen_operation = 'subtracted from'
            elif operation == 3:
                result = div(number1, number2)
                chosen_operation = 'divided with'
            elif operation == 4:
                result = mult(number1, number2)
                chosen_operation = 'multiplied with'

            print(f'{number1} {chosen_operation} {number2} is: {result}')
            print('restarting....')

    except ValueError:
        print('only numbers and "done" are accepted as input, please try again')

    except ZeroDivisionError:
        print('cannot divide by zero, please try again')

    except KeyboardInterrupt:
        goodbye()
