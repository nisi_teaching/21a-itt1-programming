"""
Exercise 2: Write a program to look for lines of the form:
New Revision: 39772

Extract the number from each of the lines using a regular expression
and the findall() method. Compute the average of the numbers and
print out the average.

Enter file:mbox.txt
38444.0323119

Enter file:mbox-short.txt
39756.9259259
"""

import re
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/docs/nikolaj_simonsen_programming_exercises/files/')

file_name = input('Enter a file name: ')

if len(file_name) < 2:
    file_name = "mbox-short.txt"

try:
    fhand = open(files_path / file_name)
    total = int(0)
    counter = int(0)

    for line in fhand:
        line = line.rstrip()
        if re.search('^New .*: [0-9]+', line): # find lines that starts with 'New ' followed by any chars : and a number
            lst = re.findall('[0-9]+', line) # find and extract all numbers from the line
            total += int(lst[0]) # cast list item 0 to an integer, add it to total
            counter += 1 # count the number of matching lines

    print(total/counter) # print the average

except FileNotFoundError:
    print("File not found", file_name)