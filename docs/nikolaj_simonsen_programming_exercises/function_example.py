## addition function
def add_two_numbers (number1, number2):
    result = number1 +  number2
    return result


# ## function call and variable assignment
# calculation = add_two_numbers(2, 2)

# ## 4 different print methods: https://www.golinuxcloud.com/python-print-variable/
# print(f'The result of calculation is: {calculation}') 
# print('the result of calculation is: ' + str(add_two_numbers(2, 2)))
# print('the result of calculation is: ', calculation)
# print('The result of calculation is: {0}'.format(calculation))


## concatenation function
# def concatenation(string1, string2, string3):
#     return f'{string1} {string2} {string3}'

# concatenated_string = concatenation('my name', 'is Nikolaj', 'Simonsen')

# print(concatenated_string)


# # Test number Function
# def is_number(arg):
#     try: 
#         int(arg)
#         float(arg)
#         return True
#     except:
#         return False

# print(is_number('Hello'))

# # grade function
def grade(score):
    error_msg = 'Bad score'
    try:
        float(score)
    except ValueError:
        return error_msg
    if score > 1.0 or score < 0.0:
        return error_msg
    elif score >= 0.9:
        return 'A'
    elif score >= 0.8:
        return 'B'
    elif score >= 0.7:
        return 'C'
    elif score >= 0.6:
        return 'D'
    elif score < 0.6:
        return 'F' 

print(grade(-3.0))
print(grade(3.0))
print(grade('perfect'))
print(grade(0.6))
print(grade(0.7))
print(grade(0.8))
print(grade(0.9))