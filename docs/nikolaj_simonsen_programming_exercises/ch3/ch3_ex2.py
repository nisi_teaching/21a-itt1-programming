'''
Exercise 2: Rewrite your pay program using try and except so that your
program handles non-numeric input gracefully by printing a message
and exiting the program. The following shows two executions of the
program:

Enter Hours: 20
Enter Rate: nine
Error, please enter numeric input

Enter Hours: forty
Error, please enter numeric input
'''

regular_hours = 40

try:
    hours = float(input('Enter Hours: '))
    rate = float(input('Enter rate: '))
    over_rate = rate * 1.5

    if hours > 40 :
        over_hours = hours - regular_hours
        print('You worked overtime this week!\n')
        print('Pay: ' + str(((over_hours)*(over_rate))+((regular_hours*rate))))
    else :
        print('Pay: ' +str(hours*rate))
except :
    print('Error, please enter numeric input')    

