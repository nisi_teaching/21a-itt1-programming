"""
Exercise 3: Write a program that reads a file and prints the letters
in decreasing order of frequency.
Your program should convert all the input to lower case and only count the letters a-z.
Your program should not count spaces, digits, punctuation, or anything other than the letters a-z.
Find text samples from several different languages and see how letter frequency varies between languages.
Compare your results with the tables at https://wikipedia.org/wiki/Letter_frequencies.

Pseudo code:

0. open file
1. read file line
2. skip empty lines
3. remove newline \n
4. remove spaces
5. remove digits
6. remove punctuation
7. convert to lower case
8. make histogram of a-z
9. order by letter frequency
10. test with language files
"""

import string
import matplotlib.pyplot as plt
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/docs/nikolaj_simonsen_programming_exercises/files/language_txt/')

# open file
file_name = input('Enter a file name: ')
if len(file_name) <=1:
    file_name = "dansk.txt"
try:
    lang_file = open(files_path / file_name, encoding="utf-8") #txt files needs to be utf-8 encoded!

    letter_histogram = dict() # to hold letter count and letter

    # read file line
    for line in lang_file:
        # skip empty line
        if not line.strip():
            continue
        else:
            line = line.rstrip("\n") # remove newline \n
            line = line.replace(" ", "") # remove spaces
            line = line.replace("–", "")  # remove weird char in danish.txt
            line = line.translate(line.maketrans("", "", string.digits)) # remove digits
            line = line.translate(line.maketrans("", "", string.punctuation)) # remove punctuation
            line = line.lower() # convert to lower case

            # make histogram of a-z, store in dictionary
            for letter in line:
                if letter not in letter_histogram:
                    letter_histogram[letter] = 1
                else:
                    letter_histogram[letter] += 1

    lang_file.close() # close the file, not needed anymore

    # convert dict to list
    sorted_list = list()
    for letter, count in list(letter_histogram.items()):
        sorted_list.append((count, letter))

    # sort the list
    sorted_list.sort()

    # lists for plot x and y axis
    letters_list = list()
    frequency_list = list()

    # add sorted list items to plot lists:
    for count, letter in sorted_list:
        letters_list.append((letter))
        frequency_list.append((count))

    # print the lists:
    # print(frequency_list)
    # print(letters_list)

    # plot the result
    plt.style.use("seaborn")
    plt.barh(letters_list, frequency_list, color="#444444", height=0.75, label=file_name + " letter frequency")
    plt.ylabel("Letters")
    plt.xlabel("Count")
    plt.title("Language letter frequency chart")
    plt.legend()
    plt.tight_layout()
    # plt.gcf().subplots_adjust(bottom=0.3)
    plt.show()

except FileNotFoundError:
    print("File not found")

except KeyboardInterrupt:
    print("Program terminated by user.....")