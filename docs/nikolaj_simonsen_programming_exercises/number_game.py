"""
A small number game. You have x number of tries to guess a number between 0 and 9
"""

# modules


import random

# global variables


computer_score = 0
human_score = 0

# functions


def game(_computer_score, _human_score):

    random_number = random.randrange(0, 10)
    turns = 3

    try:
        for turn in range(turns):
            guess = int(input('enter your guess between 0 and 9 >'))
            if guess != random_number and turn != turns-1:
                print('wrong guess, try again!')
            elif guess != random_number and turn >= turns-1:
                _computer_score = _computer_score + 1
                user_msg = input('na na I won :-P - try again? (y/n) >')
                if user_msg == 'y':
                    game(_computer_score, _human_score)
                else:
                    print('computer wins:', _computer_score, 'human wins:', _human_score, 'see ya...')
                    exit(0)  # terminate with exit code 0
            else:
                _human_score = _human_score + 1
                user_msg = input('Damn, you won!, try again? (y/n) >')
                if user_msg == 'y':
                    game(_computer_score, _human_score)
                else:
                    print('computer scored:', _computer_score, 'human scored:', _human_score, 'see ya...')
                    exit(0)
    except ValueError:
        print('That\'s not even a number..... restarting')
        game(_computer_score, _human_score)

# __main__ guard (https://stackoverflow.com/questions/419163/what-does-if-name-main-do)


if __name__ == '__main__':
    game(computer_score, human_score)
