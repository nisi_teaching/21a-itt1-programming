class Account:

    def __init__(self, name, age):
        self.name = name
        self.age = age
        self.balance = 0

    def set_balance(self, transaction):
        self.balance += transaction

    def get_balance(self): 
        return self.balance

""" 
A class to represent the capabilities of a simple bank account
Name
Age
Currency
Balance
"""

def test():
    account1 = Account(name="Simon", age=(20))
    print(f'account 1 name = {account1.name}')
    print(f'account 1 age = {account1.age}')
    print(f'account 1 balance before = {account1.get_balance()}')
    account1.set_balance(100)
    print(f'account 1 balance after = {account1.get_balance()}')

    account2 = Account(name="John", age=(22))
    print(f'account 2 name = {account2.name}')
    print(f'account 2 age = {account2.age}')

    account3 = Account(name="Nikolay", age=(19))
    print(f'account 3 name = {account3.name}')
    print(f'account 3 age = {account3.age}')

if __name__ == "__main__": # main guard
    test()
