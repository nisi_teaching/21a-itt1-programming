# chapter knowledge brainstorm on class
# class
# enheritage
# main guard
# external file 

class Person: # base class

    # constructor/init method always runs on object instantiation
    def __init__(self, age, fullname):
        self.age = age
        self.fullname = fullname

    # class methods
    def get_first_name(self):
        return self.fullname.split(' ')[0]

    def get_last_name(self):
        return self.fullname.split(' ')[1]

    def get_age(self):
        return self.age

class Vehicle: # base class
    def __init__(self, vehicle_type):
        self.vehicle_type = vehicle_type

    def get_vehicle(self):
        return self.vehicle_type

class ExtendedPerson(Person, Vehicle): # super class

    def __init__(self, age, fullname, vehicle_type, gender):
        Person.__init__(self, age, fullname)
        Vehicle.__init__(self, vehicle_type)
        self.gender = gender

    def get_gender(self): 
        return self.gender

def main():
    # instantiate object from super class
    person_1 = ExtendedPerson(30, 'Don Johnson', 'bicycle', 'male')
    person_2 = ExtendedPerson(25, 'Peter Sellers', 'car', 'male')

    # use object methods from super class
    print(
        person_1.get_first_name(), 
        person_1.get_last_name(), 
        person_1.get_age(), 
        person_1.get_vehicle()
        )

    print(
        person_2.get_first_name(), 
        person_2.get_last_name(), 
        person_2.get_age(), 
        person_2.get_vehicle()
        )
    
if __name__ == "__main__":
    main()