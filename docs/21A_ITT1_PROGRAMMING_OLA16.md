---
title: '21A ITT1 Programming'
filename: '21A_ITT1_PROGRAMMING_OLA16'
subtitle: 'OLA16'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2021-05-25
email: 'nisi@ucl.dk'
left-header: \today
right-header: OLA16
skip-toc: true
---

# Introduction

The compulsory learning activity 16 is a specification of "your own challenge".  

# Content

The hand-in is a pdf handed in on wiseflow. It needs to contain the following:

* Frontpage with name, semester, course, school logo, date and character count
* Numbered table of contents with page numbers

* A description of your chosen challenge from week 43.
    * What is the purpose of the code you are writing ?
    * How did you plan the development? Insert: Flowcharts, gitlab issues, checklists, pseudo code etc.
    * What ressources are you using to gain knowledge about the code you are developing ?
    * How are you going to test your code ?
    * What new areas of programming are you exploring through this challenge ?
    * What is the deadline ? (a date or week number)
    
* Appendix, including a link to the code and other documentation on gitlab.


I expect 3-4 pages + appendix. Note that this is an individual hand-in.

Follow-up
============

After hand-in, the lecturer will review the documents. Results are communicated through wiseflow.