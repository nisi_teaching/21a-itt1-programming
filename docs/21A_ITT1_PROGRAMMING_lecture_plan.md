---
title: '21A ITT1 PROGRAMMING'
subtitle: 'Lecture plan'
filename: '21A_ITT1_PROGRAMMING_lecture_plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2020-06-15
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
semester: 21A
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology programming, ITOiE211/ITOiE212, 21A
* Name of lecturer and date of filling in form: NISI, 2021-05-25
* Title of the course, module or project and ECTS: Programming, 6 ECTS
* Required readings, literature or technical instructions and other background material: 
    * Python For Everybody book [https://www.py4e.com/book.php](https://www.py4e.com/book.php) - [(CC license)](https://creativecommons.org/licenses/by-nd/3.0/legalcode)
    * Jetbrains PyCharm [https://www.jetbrains.com/pycharm/](https://www.jetbrains.com/pycharm/)
    * Socratica Python Programming [https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-](https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-)

See the [course website](https://eal-itt.gitlab.io/21a-itt1-programming) for detailed daily plan, links, references, exercises and so on.

| Lecturer and content | Week  | Description                                            |
|:---- |:-----: |:-------------------------------------------------- |
| NISI, Introduction to the programming course | 36    | Welcome to the programming course at IT Technology 1st semester. This week is introduction, we will setup Python and GIT on your computer and you will write your very first program, the infamous "Hello World". You will be introduced to the course lecture plan, the course structure and the course material. Python for everyone (PY4E) is the book we will be using throughout the course and this week we start with chapter 1.|  
| NISI, Variables, operators, expressions and statements | 37    | This week we start with some of the very basics of learning the Python language. What does it mean to learn a programming language and what makes a Programming language "A Language".  |  
| NISI, Conditional execution, logical operators, flow charts, exeptions | 38    | This week we are online at zoom (both classes), link to the zoom room is in the weekly plans. The topic of this week in programming are how you can make decisions in your programs, you will also learn a very valuable tool to design and document your programs, the Flowchart |
| NISI, Functions | 39    | The key to readable code is structure and abstraction of complexity. Functions is a perfect tool for both. You will learn how functions are structured, how to write function, pass arguments to functions and handle return values from them. Functions are also a way to write testable code which we will talk briefly about |
| NISI, Iteration | 40    | This week we are online at zoom (both classes), link to the zoom room is in the weekly plans. If you want to repeat anything in your programs, you need to leanr how to iterate. We talk about the different loop types as well as learning how to use them |
| NISI, Strings, Files | 41    | This week we have a lot to do, we consume 2 chapters in PY4E and they are about strings and files, 2 subjects closely related. You will learn about manipulating both strings and files in your programs. |
| NISI, Lists  | 43    | As programmers we like to be organized, we do not like unorganized data and luckily Python offers a few different datatypes to organize our data. This week is all about the list datatype, what it can and cannot do, how to use it and when to use it. | 
| NISI, Dictionaries | 44    | This week we are online at zoom (both classes), link to the zoom room is in the weekly plans. The next datatype to organize data is the dictionary type. It has different properties and syntax when compared to lists, again you will expand your programming knowledge and get the dictionary datatype in your toolbox|
| NISI, Tuples | 45    | Tuples has some very specific uses which you will learn about this week. We also have the obligatory learning activity, OLA16, scheduled. Not passing OLA's can result in not being allowed at the exam, make sure you do not miss it. Besides that it is actually a very personal OLA that has something to do with your own programming challenge :-)  |
| NISI, Regular expressions | 46    | Old and powerful, meet regular expressions. It can search and sort data in complex oneliners and you can use it in your programs as well as in your terminal. The syntax is a bit complicated but we will learn the basics of it this week. |
| NISI, Your own challenge, catch up - OLA16 1st attempt  | 47    | Sadly your lecturer is not present this week. Use this week to dive further into your own challenge or reduce your stress level by catching up on programming exercises from previous weeks. |
| NISI, Networked programs, OLA16 2nd attempt | 48    | Networked programs are the basis of IoT systems and other web dependant programs. We are going to work with HTTP, JSON and other web related protocols and technologies. You will learn how to use data from the internet in your programs |
| NISI, webservices | 49    | Webservices are programs that use web ressources as the basis of their functionality. This week we dive into them and also learn what service oriented architecture is. |
| NISI, Object-oriented programming and course evaluation | 50    | Object oriented programming (OOP) is a programming paradigm used in many places. The basis of OOP is the use of classes and objects. The OOP topic spans too wide for us to learn everything about it but we will work with the basics and give you a good foundation to explore the topic further. |
| NISI, Visualizing data | 51    | This week is without lecturer, but still very important. Visualization of data is a sought after skill and enables you to see trends in large datasets in an intuitive way. You will explore different ways to manipulate and visualize your data using Python and a few different lbiraries. |
| NISI, Databases and SQL | 01     | Up until now we have been storing data in flat files which is fine in many cases. The downside is that flat files, if large, can take a long time to search, especially if you want to combine data from different places in the file. Luckily there is a better solution, the relational database. We will learn basic modelling of a relational database using entity relation (ER) diagrams as well as the basics of SQL (Structured Query Language) to setup and query an SQLite database |
| NISI, Recap, quiz + catchup | 02     | Last lecture before the first year, part 1 exam. Please prepare for this session by going over all the learning goals in the weekly plans while asking yourself if you have any gaps. If you have gaps in your knowledge, skills or competences write down the topics and ask about them this week. We will do as many as possible during this recap. We will also do a Python quiz and I need your feedback on the programming course in a survey. Also this is the last programming lecture, maybe we need to arrange for some cake ?  |

---------------------------------------------------------


# General info about the course, module or project

The purpose of the course is to learn students the basics of programming according to the learning goals described below.

The below learning goals are high level learning goals that is translated into differentiated (3 levels) learning goals in the weekly plans.  
The purpose of translating and differentiating the learning goals is for students to actively use learning goals as a tool to evaluate themselves, during the course and to plan for recap before the exam.

## The student’s learning outcome

### Knowledge

* (LGK1) Network and server technologies in general, and the difference between physical and virtual technologies
* (LGK2) Operating systems and the differences between different systems
* (LGK3) Communication protocols and their application on different architectures
* (LGK4) Protocols, including communication protocols, their structure and what the differences and application options are
* (LGK5) Internet of Things technologies, their structure in general and selected solutions in more detail
* (LGK6) Operating systems, their characteristics and uses
* (LGK7) Programming techniques in languages of different types
* (LGK8) Algorithms and design patterns in general and in respect of the selected programming languages

### Skills

* (LGS1) Use tools and equipment in the design, development and testing of the solutions
* (LGS2) Use tools and equipment in the design, development and testing of programs
* (LGS3) Document, communicate and support programming-related solutions in respect of internal and customer-facing relationships

### Competences

* (LGC1) Acquire skills and new knowledge in the field of programming
* (LGC2) Take part in practice-oriented development processes

## Content

The subject area includes fundamentals of programming, the use of environments and data handling, as well as design, development, testing and documentation of solutions.
The course is based on the book "Python For Everybody" [https://www.py4e.com/book.php](https://www.py4e.com/book.php) and covers the basics of programming in Python. The course is emphasized on data analysis.

## Method

### PBL – Problem Based Learning 

A key design factor in the IT Technology programme is the principle that students 
develop better and more relevant competencies by tackling practical problems as 
opposed to working exclusively with theoretical textbooks.  

As a didactic concept, PBL enables the building and maintenance of motivation 
along with the simultaneous development of general and personal competencies.  

PBL is planned on the basis of three key principles: 
* Problem based activity
* Participant engagement in governance
* Situational realism in a learning environment that 
recognises accomplishment and a high level of professional competence (Stegeager, Nicolaj, Overgaard Thomassen, Anja, Stentoft, Diana, Egelund Holgaard, Jette et al. 2020).  

Theory based instruction uses dialogues during presentations and subsequent exercises.   

Various types of instruction and work are alternated, thus connecting the students’ own 
practical experience with theoretical analyses and perspectives.   

New and classic theories and methods are presented for discussion in light of the students’ own practical experience.  

The professional domain is examined in light of studies, development work, 
and new knowledge and research in the area.

### Flipped Learning

Flipped Learning is a pedagogical method that moves instructional learning from a shared environment to an individual one.  

The shared learning environment is transformed into a dynamic and interactive learning environment in which the instructor guides students in the process of learning, understanding, and applying a number of concepts and methods within a given professional subject area. Teaching is one on one, often using online 
resources.  

As a result, class time is freed up so that the instructor may provide, face to 
face with the students, a better and more individual guidance – with the aim of improving the students’ learning experience.  

One of the greatest benefits of flipped learning is the opportunity to customise and individualise the instruction for each student.  

Unfortunately, one of the greatest weaknesses of the method is that success depends on the degree to which students have done the preparatory homework before attending class (Bergmann, 
Jonathan & Sams, Aron 2015: 20-21).  

The schedule in the IT Technology programme includes time slots set aside for the students to prepare for class individually.

### Practical Lab Instruction

Lab work gives the students an opportunity to learn certain skills that are difficult to learn through other means of instruction.  

The IT Technology programme offers 
students an electronics lab and a network lab.  

Lab work supports practical skills like handling equipment and materials, building 
systems, observing and collecting data, analysing and interpreting data as well as skills in collaboration and documentation.  

While such learning opportunities are made available, learning does not automatically occur.  
The instructor must help students learn.  

Lab learning also incorporates the students’ thoughts and observations with respect to the experimental work.  

Troubleshooting by students is required when a practical exercise does not evolve as expected or does not show the anticipated results.  

Lab instruction may, as an example, include:
* A preparatory lecture in which theory is connected to the assignment at hand
* A detailed step-by-step manual for the students to follow
* A final report to be submitted by the students. 

Frequently, the instructor, and perhaps the students as well, are aware of the results of 
an experiment in advance. This type of instruction leaves students with little or no influence on what they are to investigate and how.  

There are benefits to the instructor giving more influence to the students. It increases their learning because more influence requires them to consider purposes, hypotheses, and the design of the experiment and relate these to the scientific concepts.  
In other words, the instructional program may be structured to have the students apply and reflect on professional concepts and knowledge prior to writing a report on the experimental work.  

By giving learning activites a context prior to and during the lab work, instructors give students the opportunity to think about and discuss, in depth, concepts and challenges 
with classmates and the instructor. 

## Equipment

Computer capable of running Python. Windows will be used in examples and screenshots.

## Projects with external collaborators  (proportion of students who participated)
None at this time.

## Test form/assessment
Programming includes 2 obligatory learning activities and is a part of the 1st year part 1 exam.

See the semester description on [https://www.ucl.dk/international/full-degree/study-documents#it+technology](https://www.ucl.dk/international/full-degree/study-documents#it+technology) for details on obligatory learning activities and exams.

## Study activity model

The course activities are divided in 4 categories named K1, K2, K3 and K4.  

The categories are defined as part of the Study Activity Model and is a tool for students to see the workload expected (135 efficient working hours) as well as expectations and responisibility for each category.  

Explanation of each category are in the image below as well as the distribution of activities in both hours and percentage.

In the [weekly plans](https://eal-itt.gitlab.io/21a-itt3-pcb/weekly-plans) students can get an overview of how the categories are distributed in lectures.

![study activity model](Study_Activity_Model.png)

## Other general information
None at this time.
